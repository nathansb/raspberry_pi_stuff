import time
import datetime
import json
from urllib.request import urlopen as urlRequest
from bs4 import BeautifulSoup as soup

#   What this function does:
#1) get the readings from my online thermometer stream
#2) print the readings along with a timestamp to identify them
#3) wait 10 seconds
#4) Repeat from step 1
# note that this script will run continuously until it is terminated or encounters an error
def getTemp(): 
    try:
        uClient = urlRequest(my_url)
        page_html = uClient.read()
        uClient.close()
        page_soup = soup(page_html, "html.parser")
    except:
        print("There was an error retrieving data.")
        return 0
    try:
        jsonString = page_soup
        jsonObj = json.loads(str(jsonString))
        #optional
        print(jsonObj.get("celsius"))
        print(jsonObj.get("fahrenheit"))
        print("Time: " + str(datetime.datetime.fromtimestamp(time.time())))
        time.sleep(10)
        getTemp()
    except:
        print("There was an error parsing or printing data.")
        return 0
#my_url would be the dataplicity wormhole link 
my_url = 'replace this with your own wormhole link'
getTemp()