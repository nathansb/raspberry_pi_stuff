#!/usr/bin/env python
# 5.6.2018
# ben nathanson
# This Python2.7 script implements the picamera library and Google drive API
# to make a timelapse using a Raspberry Pi 3 and upload it to the users'
# Google drive. Automating these processes makes it easier for me to remotely
# setup timelapse photography on my Raspberry Pi and get the resulting video
# from anywhere! My goals with this project were to learn more Python, 
# use an API, and writing software that is both user friendly and robust
# in the face of connectivity, hardware, and software issues. 

# This program assumes this environment:
#               Raspberry Pi 3 running Raspbian GNU/Linux 9 (stretch)
#               Raspberry Pi compatible camera (I used a kuman raspberry pi camera)
#               Python 2.7 (we can't use Python 3 here because of the API libraries)
#               PiCamera library [available at https://picamera.readthedocs.io/en/release-1.13/]

# Other notes:
# If you're running this remotely, I used Dataplicity + tmux to 
# Establish a virtual terminal that could withstand spotty wifi

# get standard libraries

try:
    import time, datetime, os, re
    from PIL import Image
    from subprocess import call
    from apiclient.discovery import build
    from httplib2 import Http
    from oauth2client import file, client, tools
except:
    print 'Default python library import error. \n Check that your pythonpath is' \
        + 'set to python 2 and your library is not corrupted. '

# camera hardware library:
# note: picamera is not part of the standard python library,
# picamera must be downloaded separately before import

try:
    import picamera
except:
    print 'Library import error. \n Check that the picamera library is installed. '

# google drive API flags

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# configure the camera
camera = picamera.PiCamera()
# flip the images vertically 
camera.vflip = True
# flip the images horizontally
camera.hflip = True
# rotate the images
camera.rotation = 270
#set the number of exposures per second
camera.framerate = 30

# configure timelapse global variables

seriesname = ''
timelapselength = 0
timebetweencaptures = 0
localtimestamp = 0
globalcounter = 0

# capture function

def capture(timelapsename, picturecount, totalcount):
    try:
        localtimestamp = time.localtime()
        time_formatted = time.strftime('%Y%m%d%H%M', localtimestamp)
        captureuniquename = timelapsename + '_' + str(picturecount) + '.jpg'
        camera.capture(captureuniquename)
        remaining = totalcount - picturecount
        print 'Captured ' + captureuniquename + ' at ' + str(time_formatted)
        print 'There are ' + str(remaining) \
        + ' captures remaining in the series. '
    except:
                print 'Capture error. '
                return 1


# cleans up all the .jpg files left behind by the timelapse
# the extension is always a .jpg, but I wanted to make tidyUp a more 
# flexible function since I intend to use it in other projects

def tidyUp(filename, extension):
    try:
        command = 'rm ' + filename + '*' + extension
        call(command, shell=True)
    except:
        print 'There has been an error attempting to erase the files. '
        exit()

# communicate with google drive, upload the video to the server

def googledriveapi(uploadfilename, cleaner):
        SCOPES = 'https://www.googleapis.com/auth/drive.file'
        store = file.Storage('storage.json')
        creds = store.get()
        if not creds or creds.invalid:
                flow = client.flow_from_clientsecrets('client_secret.json', scope=SCOPES)
                #note: one needs to register with the google drive API service to get a client secret file
                #the client_secret.json identifies your account to prevent overuse of the API
                creds = tools.run_flow(flow, store) \
                        if flags else tools.run(flow,store)   
        try:
                DRIVE = build('drive', 'v3', http=creds.authorize(Http()))
                print 'Uploading: ' + uploadfilename
                time.sleep(.5)
                metadata = {'name': uploadfilename}
                res = DRIVE.files().create(body=metadata,
                                                                   media_body=uploadfilename).execute()
                print 'Success! Uploaded ' + uploadfilename
        except:
                print 'There appears to be a connectivity problem. The program ' \
                + 'Will attempt another upload in 60 seconds'
                time.sleep(60)
                googledriveapi(uploadfilename, cleaner)
        if cleaner is True:
                tidyUp(seriesname, 'jpg')

# oversees the timing of pictures, combines a series of pictures to video
# calls the google drive upload API

def timelapse(
    uniquename,
    numberofcaptures,
    spacinginseconds,
    timestamp,
    clean,
    ):
    numbercaptured = 0
    while numbercaptured <= numberofcaptures:
        try:
            capture(uniquename, numbercaptured, numberofcaptures)
            numbercaptured = numbercaptured + 1
            time.sleep(spacinginseconds)
        except:
            print 'Capture error. '
            return 1

    # note: calling this may cause your browser to open
    # this is to establish the google drive API authentication flow
    
    try:
        camera.close()
        print 'Timelapse complete. Compiling .mp4 video . . .'
        command = 'ffmpeg -f image2 -i ' + uniquename \
            + '_%d.jpg -r 200 -s 1920:1080 ' + uniquename + '.mp4'
        call(command, shell=True)
        print 'Video compiled successfully. Preparing for google drive upload'
    except:
        camera.close()
        print 'Error processing video'
        errorlog = open('errorlog.txt', 'w')
        errorlog.write('Video processing error at '
                       + str(time.localtime()))
        errorlog.close()
        return 1
    googledriveapi(uniquename + '.mp4', clean)

# gets the name, number of pictures, and time between pictures
# if everything is in order, getinfo calls the timelapse function to
# start taking pictures

def getinfo():
    cleanupwhenfinished = False
    try:

        # it may be cleaner / efficient to do a different method than repeated .replace's here

        seriesname = str(raw_input('New timelapse name: ').replace(' ',
                         '').replace('"', '').replace('/', ''
                         ).replace(';', '').replace('\\', ''))
        timelapselength = int(input('Number of pictures: '))
        timebetweencaptures = int(input('Seconds between pictures: '))
        localtimestamp = time.localtime()
        print 'Would you like to save the individual pictures from the timelapse?'
        if str(raw_input('Please enter Y or N: ')).lower() == 'n':
            cleanupwhenfinished = True
            print 'Okay, we\'ll clean up after the video is compiled. '
        else:
            print 'Okay, we\'ll save your pictures. But be aware that they may take some space!'
    except:
        print '/nIt looks as though one of your inputs has a problem. ' \
            + 'Make sure you only entered integers for the number of pictures and seconds.'
        getinfo()
    timelapse(seriesname, timelapselength, timebetweencaptures,
              localtimestamp, cleanupwhenfinished)

getinfo()
print 'Everything terminated. Exiting python . . . '
exit()